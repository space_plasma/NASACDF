# Build Instruction

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

A simple [**Mathematica**](https://www.wolfram.com/mathematica/?source=nav) plugin for reading [**NASACDF**](https://cdf.gsfc.nasa.gov) data files.

## External dependencies

- NASA [CDF](https://cdf.gsfc.nasa.gov) library.
  The source code is automatically downloaded and made available for use.
- [Wolfram System Transfer Protocol (WSTP)](https://www.wolfram.com/wstp/) Library.
  This should be shipped your Mathematica software.

## Configure, Build, Install

Follow steps similar to these

```shell
git clone https://gitlab.com/space_plasma/NASACDF.git
cd NASACDF
mkdir build && cd build
cmake -DCMAKE_C_COMPILER=$PWD/../meta/macosx-cc \
      -DCMAKE_CXX_COMPILER=$PWD/../meta/macosx-cxx \
      -DCMAKE_INSTALL_PREFIX=$HOME/Library/Mathematica/Applications \
      -DMATHEMATICA_SYSTEM_ROOT=/Applications/Mathematica.app/Contents/SystemFiles \
      -DCMAKE_BUILD_TYPE=Release -DENABLE_IPO=On \
      -G "Unix Makefiles" ..
make
make install
```

The install prefix is where **Mathematica** can find packages.

# How to use in **Mathematica**

Interface with the library is through `CDFImport`.

Load the package and check out the usage:

```
In[1]:= <<NASACDF`

In[2]:= ?CDFImport

Out[2]= <<help message omitted...>>
```
