/*
 * Copyright (c) 2016-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

extern void CDFListVariables(const char *path);
extern void CDFImportVariable(const char *path, const char *varname);
