/*
 * This file automatically produced by /Applications/Mathematica.app/Contents/SystemFiles/Links/WSTP/DeveloperKit/MacOSX-x86-64/CompilerAdditions/wsprep from:
 *	CDFLink/CDFLink.tm
 * wsprep Revision 19 Copyright (c) Wolfram Research, Inc. 1990-2021
 */

#define WSPREP_REVISION 19

#include "wstp.h"

int WSAbort = 0;
int WSDone  = 0;
long WSSpecialCharacter = '\0';

WSLINK stdlink = 0;
WSEnvironment stdenv = 0;
WSYieldFunctionObject stdyielder = (WSYieldFunctionObject)0;
WSMessageHandlerObject stdhandler = (WSMessageHandlerObject)0;

extern int WSDoCallPacket(WSLINK);
extern int WSEvaluate( WSLINK, char *);
extern int WSEvaluateString( WSLINK, char *);

/********************************* end header *********************************/


void CDFListVariables ( const char * _tp1);

static int _tr0( WSLINK wslp)
{
	int	res = 0;
	const char * _tp1;
	if ( ! WSGetString( wslp, &_tp1) ) goto L0;
	if ( ! WSNewPacket(wslp) ) goto L1;

	CDFListVariables(_tp1);

	res = 1;
L1:	WSReleaseString(wslp, _tp1);

L0:	return res;
} /* _tr0 */


void CDFImportVariable ( const char * _tp1, const char * _tp2);

static int _tr1( WSLINK wslp)
{
	int	res = 0;
	const char * _tp1;
	const char * _tp2;
	if ( ! WSGetString( wslp, &_tp1) ) goto L0;
	if ( ! WSGetString( wslp, &_tp2) ) goto L1;
	if ( ! WSNewPacket(wslp) ) goto L2;

	CDFImportVariable(_tp1, _tp2);

	res = 1;
L2:	WSReleaseString(wslp, _tp2);
L1:	WSReleaseString(wslp, _tp1);

L0:	return res;
} /* _tr1 */


void CDFListAttributes ( const char * _tp1);

static int _tr2( WSLINK wslp)
{
	int	res = 0;
	const char * _tp1;
	if ( ! WSGetString( wslp, &_tp1) ) goto L0;
	if ( ! WSNewPacket(wslp) ) goto L1;

	CDFListAttributes(_tp1);

	res = 1;
L1:	WSReleaseString(wslp, _tp1);

L0:	return res;
} /* _tr2 */


void CDFImportAttributes ( const char * _tp1, const char * _tp2);

static int _tr3( WSLINK wslp)
{
	int	res = 0;
	const char * _tp1;
	const char * _tp2;
	if ( ! WSGetString( wslp, &_tp1) ) goto L0;
	if ( ! WSGetString( wslp, &_tp2) ) goto L1;
	if ( ! WSNewPacket(wslp) ) goto L2;

	CDFImportAttributes(_tp1, _tp2);

	res = 1;
L2:	WSReleaseString(wslp, _tp2);
L1:	WSReleaseString(wslp, _tp1);

L0:	return res;
} /* _tr3 */


void CDFImportAttributes ( const char * _tp1);

static int _tr4( WSLINK wslp)
{
	int	res = 0;
	const char * _tp1;
	if ( ! WSGetString( wslp, &_tp1) ) goto L0;
	if ( ! WSNewPacket(wslp) ) goto L1;

	CDFImportAttributes(_tp1);

	res = 1;
L1:	WSReleaseString(wslp, _tp1);

L0:	return res;
} /* _tr4 */


static struct func {
	int   f_nargs;
	int   manual;
	int   (*f_func)(WSLINK);
	const char  *f_name;
	} _tramps[5] = {
		{ 1, 0, _tr0, "CDFListVariables" },
		{ 2, 0, _tr1, "CDFImportVariable" },
		{ 1, 0, _tr2, "CDFListAttributes" },
		{ 2, 0, _tr3, "CDFImportAttributes" },
		{ 1, 0, _tr4, "CDFImportAttributes" }
		};

static const char* evalstrs[] = {
	"BeginPackage[\"NASACDF`\"]",
	(const char*)0,
	"Unprotect[CDFImport]",
	(const char*)0,
	"ClearAll[CDFImport]",
	(const char*)0,
	"CDFImport::err = \"`1`\"",
	(const char*)0,
	"Begin[\"Private`\"]",
	(const char*)0,
	"CDFImport::usage = \"CDFImport[path, \\\"Variables\\\"] lists all Varia",
	"ble names (currently zVariables only) in the cdf file.\\nCDFImport",
	"[path] is a short form to CDFImport[path, \\\"Variables\\\"].\"",
	(const char*)0,
	"CDFImport[path_String] := CDFImport[path, \"Variables\"]",
	(const char*)0,
	"CDFImport::usage = CDFImport::usage <> \"\\nCDFImport[path, {\\\"Varia",
	"bles\\\", var}] loads from the cdf file data associated with a var ",
	"variable name (currently zVariables only).\"",
	(const char*)0,
	"CDFImport::usage = CDFImport::usage <> \"\\nCDFImport[path, \\\"Attrib",
	"utes\\\"] lists all Attribute names in the cdf file.\"",
	(const char*)0,
	"CDFImport::usage = CDFImport::usage <> \"\\nCDFImport[path, {\\\"Attri",
	"butes\\\", var}] loads from the cdf file attributes associated with",
	" a var variable name (currently zVariables only) and returns the",
	"m as a list of rules, {attrname -> attrvalue}.\"",
	(const char*)0,
	"CDFImport::usage = CDFImport::usage <> \"\\nCDFImport[path, {\\\"Attri",
	"butes\\\"}] loads from the cdf file global attributes and returns t",
	"hem as a list of rules, {attrname -> attrvalue}.\"",
	(const char*)0,
	"CDFImport::noargs = \"at least a cdf path should be given\"",
	(const char*)0,
	"CDFImport[] := (Message[CDFImport::noargs]; Abort[])",
	(const char*)0,
	"CDFImport::invalidargs = \"`1` is not a valid argument(s)\"",
	(const char*)0,
	"CDFImport[path_String, args__] := (Message[CDFImport::invalidarg",
	"s, {args}]; Abort[])",
	(const char*)0,
	"CDFImport::nofile = \"`1` is not a file\"",
	(const char*)0,
	"CDFImport[path_, ___] := (Message[CDFImport::nofile, path]; Abor",
	"t[])",
	(const char*)0,
	"End[]",
	(const char*)0,
	"EndPackage[]",
	(const char*)0,
	(const char*)0
};
#define CARDOF_EVALSTRS 19

static int _definepattern( WSLINK, char*, char*, int);

static int _doevalstr( WSLINK, int);

int  _WSDoCallPacket( WSLINK, struct func[], int);


int WSInstall( WSLINK wslp)
{
	int _res;
	_res = WSConnect(wslp);
	if (_res) _res = _doevalstr( wslp, 0);
	if (_res) _res = _doevalstr( wslp, 1);
	if (_res) _res = _doevalstr( wslp, 2);
	if (_res) _res = _doevalstr( wslp, 3);
	if (_res) _res = _doevalstr( wslp, 4);
	if (_res) _res = _doevalstr( wslp, 5);
	if (_res) _res = _doevalstr( wslp, 6);
	if (_res) _res = _definepattern(wslp, (char *)"CDFImport[path_String, \"Variables\"]", (char *)"{ ExpandFileName[path] }", 0);
	if (_res) _res = _doevalstr( wslp, 7);
	if (_res) _res = _definepattern(wslp, (char *)"CDFImport[path_String, {\"Variables\", varname_String}]", (char *)"{ ExpandFileName[path], varname }", 1);
	if (_res) _res = _doevalstr( wslp, 8);
	if (_res) _res = _definepattern(wslp, (char *)"CDFImport[path_String, \"Attributes\"]", (char *)"{ ExpandFileName[path] }", 2);
	if (_res) _res = _doevalstr( wslp, 9);
	if (_res) _res = _definepattern(wslp, (char *)"CDFImport[path_String, {\"Attributes\", varname_String}]", (char *)"{ ExpandFileName[path], varname }", 3);
	if (_res) _res = _doevalstr( wslp, 10);
	if (_res) _res = _definepattern(wslp, (char *)"CDFImport[path_String, {\"Attributes\"}]", (char *)"{ ExpandFileName[path] }", 4);
	if (_res) _res = _doevalstr( wslp, 11);
	if (_res) _res = _doevalstr( wslp, 12);
	if (_res) _res = _doevalstr( wslp, 13);
	if (_res) _res = _doevalstr( wslp, 14);
	if (_res) _res = _doevalstr( wslp, 15);
	if (_res) _res = _doevalstr( wslp, 16);
	if (_res) _res = _doevalstr( wslp, 17);
	if (_res) _res = _doevalstr( wslp, 18);
	if (_res) _res = WSPutSymbol( wslp, "End");
	if (_res) _res = WSFlush( wslp);
	return _res;
} /* WSInstall */


int WSDoCallPacket( WSLINK wslp)
{
	return _WSDoCallPacket( wslp, _tramps, 5);
} /* WSDoCallPacket */

/******************************* begin trailer ********************************/

#ifndef EVALSTRS_AS_BYTESTRINGS
#	define EVALSTRS_AS_BYTESTRINGS 1
#endif


#if CARDOF_EVALSTRS
static int  _doevalstr( WSLINK wslp, int n)
{
	long bytesleft, charsleft, bytesnow;
#if !EVALSTRS_AS_BYTESTRINGS
	long charsnow;
#endif
	char **s, **p;
	char *t;

	s = (char **)evalstrs;
	while( n-- > 0){
		if( *s == 0) break;
		while( *s++ != 0){}
	}
	if( *s == 0) return 0;
	bytesleft = 0;
	charsleft = 0;
	p = s;
	while( *p){
		t = *p; while( *t) ++t;
		bytesnow = t - *p;
		bytesleft += bytesnow;
		charsleft += bytesnow;
#if !EVALSTRS_AS_BYTESTRINGS
		t = *p;
		charsleft -= WSCharacterOffset( &t, t + bytesnow, bytesnow);
		/* assert( t == *p + bytesnow); */
#endif
		++p;
	}


	WSPutNext( wslp, WSTKSTR);
#if EVALSTRS_AS_BYTESTRINGS
	p = s;
	while( *p){
		t = *p; while( *t) ++t;
		bytesnow = t - *p;
		bytesleft -= bytesnow;
		WSPut8BitCharacters( wslp, bytesleft, (unsigned char*)*p, bytesnow);
		++p;
	}
#else
	WSPut7BitCount( wslp, charsleft, bytesleft);
	p = s;
	while( *p){
		t = *p; while( *t) ++t;
		bytesnow = t - *p;
		bytesleft -= bytesnow;
		t = *p;
		charsnow = bytesnow - WSCharacterOffset( &t, t + bytesnow, bytesnow);
		/* assert( t == *p + bytesnow); */
		charsleft -= charsnow;
		WSPut7BitCharacters(  wslp, charsleft, *p, bytesnow, charsnow);
		++p;
	}
#endif
	return WSError( wslp) == WSEOK;
}
#endif /* CARDOF_EVALSTRS */


static int  _definepattern( WSLINK wslp, char *patt, char *args, int func_n)
{
	WSPutFunction( wslp, "DefineExternal", (long)3);
	  WSPutString( wslp, patt);
	  WSPutString( wslp, args);
	  WSPutInteger( wslp, func_n);
	return !WSError(wslp);
} /* _definepattern */


int _WSDoCallPacket( WSLINK wslp, struct func functable[], int nfuncs)
{
	int len;
	int n, res = 0;
	struct func* funcp;

	if( ! WSGetInteger( wslp, &n) ||  n < 0 ||  n >= nfuncs) goto L0;
	funcp = &functable[n];

	if( funcp->f_nargs >= 0
	&& ( ! WSTestHead(wslp, "List", &len)
	     || ( !funcp->manual && (len != funcp->f_nargs))
	     || (  funcp->manual && (len <  funcp->f_nargs))
	   )
	) goto L0;

	stdlink = wslp;
	res = (*funcp->f_func)( wslp);

L0:	if( res == 0)
		res = WSClearError( wslp) && WSPutSymbol( wslp, "$Failed");
	return res && WSEndPacket( wslp) && WSNewPacket( wslp);
} /* _WSDoCallPacket */


wsapi_packet WSAnswer( WSLINK wslp)
{
	wsapi_packet pkt = 0;
	int waitResult;

	while( ! WSDone && ! WSError(wslp)
		&& (waitResult = WSWaitForLinkActivity(wslp),waitResult) &&
		waitResult == WSWAITSUCCESS && (pkt = WSNextPacket(wslp), pkt) &&
		pkt == CALLPKT)
	{
		WSAbort = 0;
		if(! WSDoCallPacket(wslp))
			pkt = 0;
	}
	WSAbort = 0;
	return pkt;
} /* WSAnswer */



/*
	Module[ { me = $ParentLink},
		$ParentLink = contents of RESUMEPKT;
		Message[ MessageName[$ParentLink, "notfe"], me];
		me]
*/


static int refuse_to_be_a_frontend( WSLINK wslp)
{
	int pkt;

	WSPutFunction( wslp, "EvaluatePacket", 1);
	  WSPutFunction( wslp, "Module", 2);
	    WSPutFunction( wslp, "List", 1);
		  WSPutFunction( wslp, "Set", 2);
		    WSPutSymbol( wslp, "me");
	        WSPutSymbol( wslp, "$ParentLink");
	  WSPutFunction( wslp, "CompoundExpression", 3);
	    WSPutFunction( wslp, "Set", 2);
	      WSPutSymbol( wslp, "$ParentLink");
	      WSTransferExpression( wslp, wslp);
	    WSPutFunction( wslp, "Message", 2);
	      WSPutFunction( wslp, "MessageName", 2);
	        WSPutSymbol( wslp, "$ParentLink");
	        WSPutString( wslp, "notfe");
	      WSPutSymbol( wslp, "me");
	    WSPutSymbol( wslp, "me");
	WSEndPacket( wslp);

	while( (pkt = WSNextPacket( wslp), pkt) && pkt != SUSPENDPKT)
		WSNewPacket( wslp);
	WSNewPacket( wslp);
	return WSError( wslp) == WSEOK;
}


int WSEvaluate( WSLINK wslp, char *s)
{
	if( WSAbort) return 0;
	return WSPutFunction( wslp, "EvaluatePacket", 1L)
		&& WSPutFunction( wslp, "ToExpression", 1L)
		&& WSPutString( wslp, s)
		&& WSEndPacket( wslp);
} /* WSEvaluate */


int WSEvaluateString( WSLINK wslp, char *s)
{
	int pkt;
	if( WSAbort) return 0;
	if( WSEvaluate( wslp, s)){
		while( (pkt = WSAnswer( wslp), pkt) && pkt != RETURNPKT)
			WSNewPacket( wslp);
		WSNewPacket( wslp);
	}
	return WSError( wslp) == WSEOK;
} /* WSEvaluateString */


WSMDEFN( void, WSDefaultHandler, ( WSLINK wslp, int message, int n))
{
	switch (message){
	case WSTerminateMessage:
		WSDone = 1;
	case WSInterruptMessage:
	case WSAbortMessage:
		WSAbort = 1;
	default:
		return;
	}
}


static int _WSMain( char **argv, char **argv_end, char *commandline)
{
	WSLINK wslp;
	int err;

	if( !stdenv)
		stdenv = WSInitialize( (WSEnvironmentParameter)0);

	if( stdenv == (WSEnvironment)0) goto R0;

	if( !stdhandler)
		stdhandler = (WSMessageHandlerObject)WSDefaultHandler;


	wslp = commandline
		? WSOpenString( stdenv, commandline, &err)
		: WSOpenArgcArgv( stdenv, (int)(argv_end - argv), argv, &err);
	if( wslp == (WSLINK)0){
		WSAlert( stdenv, WSErrorString( stdenv, err));
		goto R1;
	}

	if( stdyielder) WSSetYieldFunction( wslp, stdyielder);
	if( stdhandler) WSSetMessageHandler( wslp, stdhandler);

	if( WSInstall( wslp))
		while( WSAnswer( wslp) == RESUMEPKT){
			if( ! refuse_to_be_a_frontend( wslp)) break;
		}

	WSClose( wslp);
R1:	WSDeinitialize( stdenv);
	stdenv = (WSEnvironment)0;
R0:	return !WSDone;
} /* _WSMain */


int WSMainString( char *commandline)
{
	return _WSMain( (charpp_ct)0, (charpp_ct)0, commandline);
}

int WSMainArgv( char** argv, char** argv_end) /* note not FAR pointers */
{   
	static char * far_argv[128];
	int count = 0;
	
	while(argv < argv_end)
		far_argv[count++] = *argv++;
		 
	return _WSMain( far_argv, far_argv + count, (charp_ct)0);

}


int WSMain( int argc, char **argv)
{
 	return _WSMain( argv, argv + argc, (char *)0);
}
 
