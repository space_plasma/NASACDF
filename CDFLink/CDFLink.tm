:Evaluate:      BeginPackage["NASACDF`"]


:Evaluate:      Unprotect[CDFImport]
:Evaluate:      ClearAll[CDFImport]

:Evaluate:      CDFImport::err = "`1`"


:Evaluate:      Begin["Private`"]

:Evaluate:      CDFImport::usage = "CDFImport[path, \"Variables\"] lists all Variable names (currently zVariables only) in the cdf file.\nCDFImport[path] is a short form to CDFImport[path, \"Variables\"]."
:Evaluate:      CDFImport[path_String] := CDFImport[path, "Variables"]
:Begin:
:Function:      CDFListVariables
:Pattern:       CDFImport[path_String, "Variables"]
:Arguments:     { ExpandFileName[path] }
:ArgumentTypes: { String }
:ReturnType:    Manual
:End:


:Evaluate:      CDFImport::usage = CDFImport::usage <> "\nCDFImport[path, {\"Variables\", var}] loads from the cdf file data associated with a var variable name (currently zVariables only)."
:Begin:
:Function:      CDFImportVariable
:Pattern:       CDFImport[path_String, {"Variables", varname_String}]
:Arguments:     { ExpandFileName[path], varname }
:ArgumentTypes: { String, String }
:ReturnType:    Manual
:End:




:Evaluate:      CDFImport::usage = CDFImport::usage <> "\nCDFImport[path, \"Attributes\"] lists all Attribute names in the cdf file."
:Begin:
:Function:      CDFListAttributes
:Pattern:       CDFImport[path_String, "Attributes"]
:Arguments:     { ExpandFileName[path] }
:ArgumentTypes: { String }
:ReturnType:    Manual
:End:


:Evaluate:      CDFImport::usage = CDFImport::usage <> "\nCDFImport[path, {\"Attributes\", var}] loads from the cdf file attributes associated with a var variable name (currently zVariables only) and returns them as a list of rules, {attrname -> attrvalue}."
:Begin:
:Function:      CDFImportAttributes
:Pattern:       CDFImport[path_String, {"Attributes", varname_String}]
:Arguments:     { ExpandFileName[path], varname }
:ArgumentTypes: { String, String }
:ReturnType:    Manual
:End:


:Evaluate:      CDFImport::usage = CDFImport::usage <> "\nCDFImport[path, {\"Attributes\"}] loads from the cdf file global attributes and returns them as a list of rules, {attrname -> attrvalue}."
:Begin:
:Function:      CDFImportAttributes
:Pattern:       CDFImport[path_String, {"Attributes"}]
:Arguments:     { ExpandFileName[path] }
:ArgumentTypes: { String }
:ReturnType:    Manual
:End:




:Evaluate:      CDFImport::noargs = "at least a cdf path should be given"
:Evaluate:      CDFImport[] := (Message[CDFImport::noargs]; Abort[])

:Evaluate:      CDFImport::invalidargs = "`1` is not a valid argument(s)"
:Evaluate:      CDFImport[path_String, args__] := (Message[CDFImport::invalidargs, {args}]; Abort[])

:Evaluate:      CDFImport::nofile = "`1` is not a file"
:Evaluate:      CDFImport[path_, ___] := (Message[CDFImport::nofile, path]; Abort[])

:Evaluate:      End[]


:Evaluate:      EndPackage[]