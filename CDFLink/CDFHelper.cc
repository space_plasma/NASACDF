/*
 * Copyright (c) 2016-2024, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "CDFHelper.h"
#include <array>
#include <iterator>
#include <numeric>
#include <ranges>
#include <stdexcept>

CDFFile::~CDFFile()
{
    if (m_id)
        CDFcloseCDF(*m_id);
}
CDFFile::CDFFile(std::filesystem::path const path)
: m_id{ std::make_unique<CDFid>() }
{
    if (CDF_OK != CDFopenCDF(path.c_str(), m_id.get()))
        throw std::runtime_error{ sprint(__PRETTY_FUNCTION__, ": failed to open `", path, "`") };
}

long CDFFile::n_zvars() const
{
    if (long number_of_zvars{}; CDF_OK == CDFgetNumzVars(*m_id, &number_of_zvars))
        return number_of_zvars;
    throw std::runtime_error{ __PRETTY_FUNCTION__ };
}
auto CDFFile::zvar_names() const -> std::vector<std::string>
{
    std::vector<std::string> names;
    for (long const i : std::views::iota(0L, this->n_zvars())) {
        std::array<char, CDF_VAR_NAME_LEN256> name{};
        CDFgetzVarName(*m_id, i, name.data());
        names.push_back(name.data());
    }
    return names;
}
auto CDFFile::zvar(const char *varname) const -> CDFzVar
{
    if (CDF_OK == CDFconfirmzVarExistence(*m_id, const_cast<char *>(varname)))
        return CDFzVar{ this, CDFgetVarNum(*m_id, const_cast<char *>(varname)) };
    throw std::runtime_error{ sprint(__PRETTY_FUNCTION__, ": no variable named `", varname, "`") };
}

long CDFFile::n_attrs() const
{
    if (long number_of_attributes{}; CDF_OK == CDFgetNumAttributes(*m_id, &number_of_attributes))
        return number_of_attributes;
    throw std::runtime_error{ __PRETTY_FUNCTION__ };
}
auto CDFFile::attr_names() const -> std::vector<std::string>
{
    std::vector<std::string> names;
    for (long const i : std::views::iota(0L, this->n_attrs())) {
        std::array<char, CDF_ATTR_NAME_LEN256> name{};
        CDFgetAttrName(*m_id, i, name.data());
        names.push_back(name.data());
    }
    return names;
}
auto CDFFile::attrs() const -> std::vector<CDFgAttr>
{
    std::vector<CDFgAttr> attrs;
    for (long const i : std::views::iota(0L, this->n_attrs())) {
        long scope;
        if (CDF_OK != CDFgetAttrScope(*m_id, i, &scope))
            throw std::runtime_error{ __PRETTY_FUNCTION__ };

        if (GLOBAL_SCOPE == scope)
            attrs.emplace_back(CDFgAttr{ this, i });
    }
    return attrs;
}

CDFgAttr::CDFgAttr(CDFFile const *cdf, long const &gattr_num)
: m_cdf{ cdf }, m_num{ gattr_num }
{
    long scope{}, maxgEntry{}, maxrEntry{}, maxzEntry{};
    if (std::array<char, CDF_ATTR_NAME_LEN256> name{};
        CDF_OK == CDFinquireAttr(m_cdf->handle(), m_num, name.data(), &scope, &maxgEntry, &maxrEntry, &maxzEntry))
        m_name = name.data();
    else
        throw std::invalid_argument{ __PRETTY_FUNCTION__ };

    // iterate over entries
    //
    for (long const i : std::views::iota(0L, maxgEntry + 1)) {
        if (NO_SUCH_ENTRY != CDFconfirmgEntryExistence(m_cdf->handle(), m_num, i))
            CDFinquireAttrgEntry(m_cdf->handle(), m_num, i, &m_entry_map[i].first, &m_entry_map[i].second);
    }
}

auto CDFgAttr::operator>>(WSLINK link) const -> CDFgAttr const &
{
    WSPutFunction(link, "Rule", 2);
    {
        WSPutString(link, m_name.c_str());
        WSPutFunction(link, "List", int(m_entry_map.size()));
        for (auto const &entry : m_entry_map) {
            long const entry_num = entry.first;
            long const type      = entry.second.first;
            long const n_elems   = entry.second.second;

            if (CDF_CHAR == type || CDF_UCHAR == type) {
                auto const buffer = [this, entry_num, type, n_elems]() -> std::vector<int> {
                    std::vector<unsigned char> tmp;
                    load_entry(entry_num, type, n_elems, tmp);
                    return { tmp.begin(), tmp.end() };
                }();

                WSPutFunction(link, "FromCharacterCode", 1);
                WSPutIntegerList(link, buffer.data(), long(buffer.size()));

            } else if (type == CDF_INT1 || type == CDF_BYTE || type == CDF_INT2 || type == CDF_INT4 || type == CDF_INT8 || type == CDF_UINT1 || type == CDF_UINT2 || type == CDF_UINT4) {
                std::vector<long> buffer;
                load_entry(entry_num, type, n_elems, buffer);

                long const dim = long(buffer.size());
                WSPutLongIntegerArray(link, buffer.data(), &dim, nullptr, 1);

            } else if (type == CDF_REAL8 || type == CDF_DOUBLE || type == CDF_REAL4 || type == CDF_FLOAT) {
                std::vector<double> buffer;
                load_entry(entry_num, type, n_elems, buffer);

                long const dim = long(buffer.size());
                WSPutDoubleArray(link, buffer.data(), &dim, nullptr, 1);

            } else if (type == CDF_EPOCH || type == CDF_EPOCH16 || type == CDF_TIME_TT2000) {
                std::vector<epoch_t> buffer;
                load_entry(entry_num, type, n_elems, buffer);

                int const lvlspec = -2;
                WSPutFunction(link, "Map", 3);
                {
                    WSPutFunction(link, "Function", 2);
                    {
                        WSPutSymbol(link, "dl");
                        WSPutFunction(link, "Append", 2);
                        {
                            WSPutFunction(link, "Round", 1), WSPutFunction(link, "Most", 1), WSPutSymbol(link, "dl");
                            WSPutFunction(link, "Last", 1), WSPutSymbol(link, "dl");
                        }
                    }
                    std::array<long, 2> const dims{ long(buffer.size()), sizeof(epoch_t) / sizeof(epoch_t::year) };
                    WSPutDoubleArray(link, &buffer.front().year, dims.data(), nullptr, 2);
                    WSPutIntegerList(link, &lvlspec, 1);
                }

            } else {
                throw std::runtime_error{ sprint(__PRETTY_FUNCTION__, ": type of ", entry_num, "th entry of `", m_name, "` attribute is not supported") };
            }
        }
    }
    return *this;
}

void CDFgAttr::load_entry(long const entry_num, long const type, long const n_elems, std::vector<unsigned char> &buffer) const
{
    if (CDF_CHAR == type || CDF_UCHAR == type) {
        buffer.resize(std::size_t(n_elems));
        load_entry(entry_num, buffer);
    } else {
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": ", entry_num, "th entry of `", m_name, "` attribute is not a character type") };
    }
}
void CDFgAttr::load_entry(long const entry_num, long const type, long const n_elems, std::vector<long> &buffer) const
{
    if (CDF_INT1 == type || CDF_BYTE == type) {
        std::vector<char> tmp(static_cast<std::size_t>(n_elems));
        load_entry(entry_num, tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_INT2 == type) {
        std::vector<short> tmp(static_cast<std::size_t>(n_elems));
        load_entry(entry_num, tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_INT4 == type) {
        std::vector<int> tmp(static_cast<std::size_t>(n_elems));
        load_entry(entry_num, tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_INT8 == type) {
        buffer.resize(std::size_t(n_elems));
        load_entry(entry_num, buffer);
    } else if (CDF_UINT1 == type) {
        std::vector<unsigned char> tmp(static_cast<std::size_t>(n_elems));
        load_entry(entry_num, tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_UINT2 == type) {
        std::vector<unsigned short> tmp(static_cast<std::size_t>(n_elems));
        load_entry(entry_num, tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_UINT4 == type) {
        std::vector<unsigned int> tmp(static_cast<std::size_t>(n_elems));
        load_entry(entry_num, tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else {
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": ", entry_num, "th entry of `", m_name, "` attribute is not a integer type") };
    }
}
void CDFgAttr::load_entry(long const entry_num, long const type, long const n_elems, std::vector<double> &buffer) const
{
    if (CDF_FLOAT == type || CDF_REAL4 == type) {
        std::vector<float> tmp(static_cast<std::size_t>(n_elems));
        load_entry(entry_num, tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_DOUBLE == type || CDF_REAL8 == type) {
        buffer.resize(std::size_t(n_elems));
        load_entry(entry_num, buffer);
    } else {
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": ", entry_num, "th entry of `", m_name, "` attribute is not a real type") };
    }
}
void CDFgAttr::load_entry(long const entry_num, long const type, long const n_elems, std::vector<epoch_t> &buffer) const
{
    if (CDF_EPOCH == type) {
        std::vector<double> tmp(static_cast<std::size_t>(n_elems));
        load_entry(entry_num, tmp);
        buffer.clear();
        for (auto const &epoch : tmp) {
            long year, month, day, hour, minute, second, msec;
            EPOCHbreakdown(epoch, &year, &month, &day, &hour, &minute, &second, &msec);
            buffer.push_back({ year, month, day, hour, minute, second + msec * 1e-3 });
        }
    } else if (CDF_EPOCH16 == type) {
        std::vector<std::pair<double, double>> tmp(static_cast<std::size_t>(n_elems));
        load_entry(entry_num, tmp);
        buffer.clear();
        for (auto epoch16 : tmp) {
            long year, month, day, hour, minute, second, msec, usec, nsec, psec;
            EPOCH16breakdown(&epoch16.first, &year, &month, &day, &hour, &minute, &second, &msec, &usec, &nsec, &psec);
            buffer.push_back({ year, month, day, hour, minute, second + msec * 1e-3 + usec * 1e-6 + nsec * 1e-9 + psec * 1e-12 });
        }
    } else if (CDF_TIME_TT2000 == type) {
        std::vector<long long> tmp(static_cast<std::size_t>(n_elems));
        load_entry(entry_num, tmp);
        buffer.clear();
        for (auto const &tt2000 : tmp) {
            double year, month, day, hour, minute, second, msec, usec, nsec;
            CDF_TT2000_to_UTC_parts(tt2000, &year, &month, &day, &hour, &minute, &second, &msec, &usec, &nsec);
            buffer.push_back({ year, month, day, hour, minute, second + msec * 1e-3 + usec * 1e-6 + nsec * 1e-9 });
        }
    } else {
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": ", entry_num, "th entry of `", m_name, "` attribute is not a epoch type nor a TT2000 type") };
    }
}

template <class T>
void CDFgAttr::load_entry(const long entry_num, std::vector<T> &buffer) const
{
    if (buffer.empty())
        return;

    if (CDF_OK != CDFgetAttrgEntry(m_cdf->handle(), m_num, entry_num, buffer.data()))
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": failed to load ", entry_num, "th entry of `", m_name, "` variable data") };
}

CDFzVar::CDFzVar(CDFFile const *cdf, long const &var_num)
: m_cdf{ cdf }, m_num{ var_num }
{
    long numDims{}, recVary{}, maxRec{};
    //
    std::array<long, CDF_MAX_DIMS> dimSizes{};
    std::array<long, CDF_MAX_DIMS> dimVarys{};

    if (std::array<char, CDF_VAR_NAME_LEN256> name{};
        CDF_OK == CDFinquirezVar(m_cdf->handle(), m_num, name.data(), &m_type, &m_n_elements, &numDims, dimSizes.data(), &recVary, dimVarys.data()))
        m_name = name.data();
    else
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": failed in the call `CDFinquirezVar`") };

    if (CDF_OK != CDFgetzVarMaxWrittenRecNum(m_cdf->handle(), m_num, &maxRec))
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": failed in the call `CDFgetzVarMaxWrittenRecNum`") };

    m_dim_sizes.push_back(maxRec + 1);
    m_dim_sizes.insert(m_dim_sizes.end(), begin(dimSizes), std::next(begin(dimSizes), numDims));

    m_variances.push_back(recVary);
    m_variances.insert(m_variances.end(), begin(dimVarys), std::next(begin(dimVarys), numDims));
}

auto CDFzVar::buffer_size() const noexcept -> std::size_t
{
    std::vector<std::size_t> const dims{ m_dim_sizes.begin(), m_dim_sizes.end() };
    return std::accumulate(dims.begin(), dims.end(), std::size_t{ 1 }, std::multiplies<>{});
}

auto CDFzVar::operator>>(WSLINK link) const -> CDFzVar const &
{
    if (CDF_CHAR == m_type || CDF_UCHAR == m_type) {
        auto const buffer = [this]() -> std::vector<long> {
            std::vector<unsigned char> tmp;
            *this >> tmp;
            return { tmp.begin(), tmp.end() };
        }();
        std::vector<long> dims{ std::next(m_dim_sizes.begin(), m_variances.front() ? 0 : 1), m_dim_sizes.end() };
        dims.push_back(m_n_elements);

        if (buffer.empty()) {
            WSPutFunction(link, "List", 0);
        } else {
            int const level = -2;
            WSPutFunction(link, "Map", 3);
            {
                WSPutSymbol(link, "FromCharacterCode");
                WSPutLongIntegerArray(link, buffer.data(), dims.data(), nullptr, long(dims.size()));
                WSPutIntegerList(link, &level, 1);
            }
        }
    } else if (m_type == CDF_INT1 || m_type == CDF_BYTE || m_type == CDF_INT2 || m_type == CDF_INT4 || m_type == CDF_INT8 || m_type == CDF_UINT1 || m_type == CDF_UINT2 || m_type == CDF_UINT4) {
        std::vector<long> buffer;
        *this >> buffer;
        std::vector<long> dims{ std::next(m_dim_sizes.begin(), m_variances.front() ? 0 : 1), m_dim_sizes.end() };

        WSPutLongIntegerArray(link, buffer.data(), dims.data(), nullptr, long(dims.size()));

    } else if (m_type == CDF_REAL8 || m_type == CDF_DOUBLE || m_type == CDF_REAL4 || m_type == CDF_FLOAT) {
        std::vector<double> buffer;
        *this >> buffer;
        std::vector<long> dims{ std::next(m_dim_sizes.begin(), m_variances.front() ? 0 : 1), m_dim_sizes.end() };

        WSPutDoubleArray(link, buffer.data(), dims.data(), nullptr, long(dims.size()));

    } else if (m_type == CDF_EPOCH || m_type == CDF_EPOCH16 || m_type == CDF_TIME_TT2000) {
        std::vector<epoch_t> buffer;
        *this >> buffer;
        std::vector<long> dims{ std::next(m_dim_sizes.begin(), m_variances.front() ? 0 : 1), m_dim_sizes.end() };
        dims.push_back(sizeof(epoch_t) / sizeof(epoch_t::year));

        if (buffer.empty()) {
            WSPutFunction(link, "List", 0);
        } else {
            int const lvlspec = -2;
            WSPutFunction(link, "Map", 3);
            {
                WSPutFunction(link, "Function", 2);
                {
                    WSPutSymbol(link, "dl");
                    WSPutFunction(link, "Append", 2);
                    {
                        WSPutFunction(link, "Round", 1), WSPutFunction(link, "Most", 1), WSPutSymbol(link, "dl");
                        WSPutFunction(link, "Last", 1), WSPutSymbol(link, "dl");
                    }
                }
                WSPutDoubleArray(link, &buffer.front().year, dims.data(), nullptr, long(dims.size()));
                WSPutIntegerList(link, &lvlspec, 1);
            }
        }
    } else {
        throw std::runtime_error{ sprint(__PRETTY_FUNCTION__, ": type of `", m_name, "` is not supported") };
    }

    return *this;
}

auto CDFzVar::operator>>(std::vector<unsigned char> &buffer) const -> CDFzVar const &
{
    if (CDF_CHAR == m_type || CDF_UCHAR == m_type) {
        buffer.resize(buffer_size() * unsigned(m_n_elements));
        load(buffer);
    } else {
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": `", m_name, "` is not a character type") };
    }

    return *this;
}
auto CDFzVar::operator>>(std::vector<long> &buffer) const -> CDFzVar const &
{
    if (CDF_INT1 == m_type || CDF_BYTE == m_type) {
        std::vector<char> tmp(buffer_size());
        load(tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_INT2 == m_type) {
        std::vector<short> tmp(buffer_size());
        load(tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_INT4 == m_type) {
        std::vector<int> tmp(buffer_size());
        load(tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_INT8 == m_type) {
        buffer.resize(buffer_size());
        load(buffer);
    } else if (CDF_UINT1 == m_type) {
        std::vector<unsigned char> tmp(buffer_size());
        load(tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_UINT2 == m_type) {
        std::vector<unsigned short> tmp(buffer_size());
        load(tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_UINT4 == m_type) {
        std::vector<unsigned int> tmp(buffer_size());
        load(tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else {
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": `", m_name, "` is not a integer type") };
    }

    return *this;
}
auto CDFzVar::operator>>(std::vector<double> &buffer) const -> CDFzVar const &
{
    if (CDF_FLOAT == m_type || CDF_REAL4 == m_type) {
        std::vector<float> tmp(buffer_size());
        load(tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_DOUBLE == m_type || CDF_REAL8 == m_type) {
        buffer.resize(buffer_size());
        load(buffer);
    } else {
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": `", m_name, "` is not a real type") };
    }

    return *this;
}
auto CDFzVar::operator>>(std::vector<epoch_t> &buffer) const -> CDFzVar const &
{
    if (CDF_EPOCH == m_type) {
        std::vector<double> tmp(buffer_size());
        load(tmp);
        buffer.clear();
        for (auto const &epoch : tmp) {
            long year, month, day, hour, minute, second, msec;
            EPOCHbreakdown(epoch, &year, &month, &day, &hour, &minute, &second, &msec);
            buffer.push_back({ year, month, day, hour, minute, second + msec * 1e-3 });
        }
    } else if (CDF_EPOCH16 == m_type) {
        std::vector<std::pair<double, double>> tmp(buffer_size());
        load(tmp);
        buffer.clear();
        for (auto epoch16 : tmp) {
            long year, month, day, hour, minute, second, msec, usec, nsec, psec;
            EPOCH16breakdown(&epoch16.first, &year, &month, &day, &hour, &minute, &second, &msec, &usec, &nsec, &psec);
            buffer.push_back({ year, month, day, hour, minute, second + msec * 1e-3 + usec * 1e-6 + nsec * 1e-9 + psec * 1e-12 });
        }
    } else if (CDF_TIME_TT2000 == m_type) {
        std::vector<long long> tmp(buffer_size());
        load(tmp);
        buffer.clear();
        for (auto const &tt2000 : tmp) {
            double year, month, day, hour, minute, second, msec, usec, nsec;
            CDF_TT2000_to_UTC_parts(tt2000, &year, &month, &day, &hour, &minute, &second, &msec, &usec, &nsec);
            buffer.push_back({ year, month, day, hour, minute, second + msec * 1e-3 + usec * 1e-6 + nsec * 1e-9 });
        }
    } else {
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": `", m_name, "` is not a epoch type nor a TT2000 type") };
    }

    return *this;
}

template <class T>
void CDFzVar::load(std::vector<T> &buffer) const
{
    if (buffer.empty())
        return;

    if (CDF_OK != CDFgetzVarAllRecordsByVarID(m_cdf->handle(), m_num, buffer.data()))
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": failed to load `", m_name, "` variable data") };
}

std::vector<CDFvAttr> CDFzVar::attrs() const
{
    std::vector<CDFvAttr> attrs;
    for (long const i : std::views::iota(0L, m_cdf->n_attrs())) {
        long scope;
        if (CDF_OK != CDFgetAttrScope(m_cdf->handle(), i, &scope))
            throw std::runtime_error{ __PRETTY_FUNCTION__ };

        if (VARIABLE_SCOPE == scope && NO_SUCH_ENTRY != CDFconfirmzEntryExistence(m_cdf->handle(), i, m_num))
            attrs.emplace_back(CDFvAttr{ m_cdf, i, m_num });
    }
    return attrs;
}

CDFvAttr::CDFvAttr(CDFFile const *cdf, long const &attr_num, long const &entry_num)
: m_cdf{ cdf }, m_attr_num{ attr_num }, m_entry_num{ entry_num }
{
    if (std::array<char, CDF_ATTR_NAME_LEN256> name{}; CDF_OK == CDFgetAttrName(m_cdf->handle(), m_attr_num, name.data()))
        m_name = name.data();
    else
        throw std::invalid_argument{ __PRETTY_FUNCTION__ };

    CDFinquireAttrzEntry(m_cdf->handle(), m_attr_num, m_entry_num, &m_type, &m_n_elems);
}

auto CDFvAttr::operator>>(WSLINK link) const -> CDFvAttr const &
{
    WSPutFunction(link, "Rule", 2);
    {
        WSPutString(link, m_name.c_str());
        if (CDF_CHAR == m_type || CDF_UCHAR == m_type) {
            auto const buffer = [this]() -> std::vector<int> {
                std::vector<unsigned char> tmp;
                *this >> tmp;
                return { tmp.begin(), tmp.end() };
            }();

            if (buffer.empty()) {
                WSPutFunction(link, "List", 0);
            } else {
                WSPutFunction(link, "FromCharacterCode", 1);
                WSPutIntegerList(link, buffer.data(), long(buffer.size()));
            }
        } else if (m_type == CDF_INT1 || m_type == CDF_BYTE || m_type == CDF_INT2 || m_type == CDF_INT4 || m_type == CDF_INT8 || m_type == CDF_UINT1 || m_type == CDF_UINT2 || m_type == CDF_UINT4) {
            std::vector<long> buffer;
            *this >> buffer;

            long const dim = long(buffer.size());
            WSPutLongIntegerArray(link, buffer.data(), &dim, nullptr, 1);

        } else if (m_type == CDF_REAL8 || m_type == CDF_DOUBLE || m_type == CDF_REAL4 || m_type == CDF_FLOAT) {
            std::vector<double> buffer;
            *this >> buffer;

            long const dim = long(buffer.size());
            WSPutDoubleArray(link, buffer.data(), &dim, nullptr, 1);

        } else if (m_type == CDF_EPOCH || m_type == CDF_EPOCH16 || m_type == CDF_TIME_TT2000) {
            std::vector<epoch_t> buffer;
            *this >> buffer;

            if (buffer.empty()) {
                WSPutFunction(link, "List", 0);
            } else {
                int const lvlspec = -2;
                WSPutFunction(link, "Map", 3);
                {
                    WSPutFunction(link, "Function", 2);
                    {
                        WSPutSymbol(link, "dl");
                        WSPutFunction(link, "Append", 2);
                        {
                            WSPutFunction(link, "Round", 1), WSPutFunction(link, "Most", 1), WSPutSymbol(link, "dl");
                            WSPutFunction(link, "Last", 1), WSPutSymbol(link, "dl");
                        }
                    }
                    std::array<long, 2> const dims{ long(buffer.size()), sizeof(epoch_t) / sizeof(epoch_t::year) };
                    WSPutDoubleArray(link, &buffer.front().year, dims.data(), nullptr, 2);
                    WSPutIntegerList(link, &lvlspec, 1);
                }
            }
        } else {
            throw std::runtime_error{ sprint(__PRETTY_FUNCTION__, ": type of ", m_entry_num, "th entry of `", m_name, "` attribute is not supported") };
        }
    }

    return *this;
}

auto CDFvAttr::operator>>(std::vector<unsigned char> &buffer) const -> CDFvAttr const &
{
    if (CDF_CHAR == m_type || CDF_UCHAR == m_type) {
        buffer.resize(std::size_t(m_n_elems));
        load(buffer);
    } else {
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": ", m_entry_num, "th entry of `", m_name, "` attribute is not a character type") };
    }

    return *this;
}
auto CDFvAttr::operator>>(std::vector<long> &buffer) const -> CDFvAttr const &
{
    if (CDF_INT1 == m_type || CDF_BYTE == m_type) {
        std::vector<char> tmp(static_cast<std::size_t>(m_n_elems));
        load(tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_INT2 == m_type) {
        std::vector<short> tmp(static_cast<std::size_t>(m_n_elems));
        load(tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_INT4 == m_type) {
        std::vector<int> tmp(static_cast<std::size_t>(m_n_elems));
        load(tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_INT8 == m_type) {
        buffer.resize(std::size_t(m_n_elems));
        load(buffer);
    } else if (CDF_UINT1 == m_type) {
        std::vector<unsigned char> tmp(static_cast<std::size_t>(m_n_elems));
        load(tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_UINT2 == m_type) {
        std::vector<unsigned short> tmp(static_cast<std::size_t>(m_n_elems));
        load(tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_UINT4 == m_type) {
        std::vector<unsigned int> tmp(static_cast<std::size_t>(m_n_elems));
        load(tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else {
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": ", m_entry_num, "th entry of `", m_name, "` attribute is not a integer type") };
    }

    return *this;
}
auto CDFvAttr::operator>>(std::vector<double> &buffer) const -> CDFvAttr const &
{
    if (CDF_FLOAT == m_type || CDF_REAL4 == m_type) {
        std::vector<float> tmp(static_cast<std::size_t>(m_n_elems));
        load(tmp);
        buffer.clear();
        buffer.insert(buffer.end(), tmp.begin(), tmp.end());
    } else if (CDF_DOUBLE == m_type || CDF_REAL8 == m_type) {
        buffer.resize(std::size_t(m_n_elems));
        load(buffer);
    } else {
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": ", m_entry_num, "th entry of `", m_name, "` attribute is not a real type") };
    }

    return *this;
}
auto CDFvAttr::operator>>(std::vector<epoch_t> &buffer) const -> CDFvAttr const &
{
    if (CDF_EPOCH == m_type) {
        std::vector<double> tmp(static_cast<std::size_t>(m_n_elems));
        load(tmp);
        buffer.clear();
        for (auto const &epoch : tmp) {
            long year, month, day, hour, minute, second, msec;
            EPOCHbreakdown(epoch, &year, &month, &day, &hour, &minute, &second, &msec);
            buffer.push_back({ year, month, day, hour, minute, second + msec * 1e-3 });
        }
    } else if (CDF_EPOCH16 == m_type) {
        std::vector<std::pair<double, double>> tmp(static_cast<std::size_t>(m_n_elems));
        load(tmp);
        buffer.clear();
        for (auto epoch16 : tmp) {
            long year, month, day, hour, minute, second, msec, usec, nsec, psec;
            EPOCH16breakdown(&epoch16.first, &year, &month, &day, &hour, &minute, &second, &msec, &usec, &nsec, &psec);
            buffer.push_back({ year, month, day, hour, minute, second + msec * 1e-3 + usec * 1e-6 + nsec * 1e-9 + psec * 1e-12 });
        }
    } else if (CDF_TIME_TT2000 == m_type) {
        std::vector<long long> tmp(static_cast<std::size_t>(m_n_elems));
        load(tmp);
        buffer.clear();
        for (auto const &tt2000 : tmp) {
            double year, month, day, hour, minute, second, msec, usec, nsec;
            CDF_TT2000_to_UTC_parts(tt2000, &year, &month, &day, &hour, &minute, &second, &msec, &usec, &nsec);
            buffer.push_back({ year, month, day, hour, minute, second + msec * 1e-3 + usec * 1e-6 + nsec * 1e-9 });
        }
    } else {
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": ", m_entry_num, "th entry of `", m_name, "` attribute is not a epoch type nor a TT2000 type") };
    }

    return *this;
}

template <class T>
void CDFvAttr::load(std::vector<T> &buffer) const
{
    if (buffer.empty())
        return;

    if (CDF_OK != CDFgetAttrzEntry(m_cdf->handle(), m_attr_num, m_entry_num, buffer.data()))
        throw std::invalid_argument{ sprint(__PRETTY_FUNCTION__, ": failed to load ", m_entry_num, "th entry of `", m_name, "` variable data") };
}
