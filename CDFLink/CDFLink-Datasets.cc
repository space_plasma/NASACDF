/*
 * Copyright (c) 2016-2024, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "CDFLink-Datasets.h"
#include "CDFHelper.h"

void CDFListVariables(const char *path)
try {
    [cdf = CDFFile{ path }] {
        std::vector<std::string> const names = cdf.zvar_names();
        {
            WSPutFunction(stdlink, "List", int(names.size()));
            for (auto const &name : names) {
                WSPutString(stdlink, name.c_str());
            }
        }
    }();
} catch (std::exception &e) {
    std::string msg = sprint("Message[CDFImport::err, \"", e.what(), "\"]");
    WSEvaluateString(stdlink, msg.data());
    WSPutFunction(stdlink, "Abort", 0);
}

void CDFImportVariable(const char *path, const char *varname)
try {
    [cdf = CDFFile{ path }, varname] {
        cdf.zvar(varname) >> stdlink;
    }();
} catch (std::exception &e) {
    std::string msg = sprint("Message[CDFImport::err, \"", e.what(), "\"]");
    WSEvaluateString(stdlink, msg.data());
    WSPutFunction(stdlink, "Abort", 0);
}
