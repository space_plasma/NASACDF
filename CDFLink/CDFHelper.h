/*
 * Copyright (c) 2016-2024, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <cdf.h>
#include <cstdlib>
#include <filesystem>
#include <map>
#include <memory>
#include <ostream>
#include <sstream>
#include <string>
#include <utility>
#include <vector>
#include <wstp.h>

// Time representation
//
struct [[nodiscard]] epoch_t {
    double year{};
    double month{};
    double day{};
    double hour{};
    double minute{};
    double second{};

    constexpr explicit epoch_t() noexcept = default;
    constexpr epoch_t(double y, double m, double d, double h, double min, double sec) noexcept
    : year{ y }, month{ m }, day{ d }, hour{ h }, minute{ min }, second{ sec } {}
    constexpr epoch_t(long y, long m, long d, long h, long min, double sec) noexcept
    : year{ double(y) }
    , month{ double(m) }
    , day{ double(d) }
    , hour{ double(h) }
    , minute{ double(min) }
    , second{ sec } {}
};

// CDFFile
//
class CDFzVar;
class CDFgAttr;
class [[nodiscard]] CDFFile {
    std::unique_ptr<CDFid> m_id;

public:
    [[nodiscard]] CDFid const &handle() const noexcept { return *m_id; }

    ~CDFFile();
    explicit CDFFile() noexcept = default;
    explicit CDFFile(std::filesystem::path);

    CDFFile(CDFFile const &)                = delete;
    CDFFile &operator=(CDFFile const &)     = delete;
    CDFFile(CDFFile &&) noexcept            = default;
    CDFFile &operator=(CDFFile &&) noexcept = default;

    void swap(CDFFile &other) noexcept { std::swap(m_id, other.m_id); }

    [[nodiscard]] long n_zvars() const;
    [[nodiscard]] auto zvar_names() const -> std::vector<std::string>;
    [[nodiscard]] auto zvar(char const *varname) const -> CDFzVar;

    [[nodiscard]] long n_attrs() const;
    [[nodiscard]] auto attr_names() const -> std::vector<std::string>;
    [[nodiscard]] auto attrs() const -> std::vector<CDFgAttr>;
};

// CDFgAttr
//
class [[nodiscard]] CDFgAttr {
    friend CDFFile;
    using map_t = std::map<long /*entry_num*/, std::pair<long /*type*/, long /*n_elems*/>>;

    CDFFile const *m_cdf;
    long           m_num;
    std::string    m_name;
    map_t          m_entry_map;

    explicit CDFgAttr(CDFFile const *cdf, long const &attr_num);

public:
    auto operator>>(WSLINK link) const -> CDFgAttr const &;

private:
    void load_entry(long entry_num, long type, long n_elems, std::vector<unsigned char> &buffer) const;
    void load_entry(long entry_num, long type, long n_elems, std::vector<long> &buffer) const;
    void load_entry(long entry_num, long type, long n_elems, std::vector<double> &buffer) const;
    void load_entry(long entry_num, long type, long n_elems, std::vector<epoch_t> &buffer) const;

    template <class T>
    void load_entry(long entry_num, std::vector<T> &buffer) const;
};

// CDFVar
//
class CDFvAttr;
class [[nodiscard]] CDFzVar {
    friend CDFFile;

    CDFFile const    *m_cdf;
    long              m_num;
    std::string       m_name;
    long              m_type{};
    long              m_n_elements{};
    std::vector<long> m_dim_sizes; // including record
    std::vector<long> m_variances; // including record

    explicit CDFzVar(CDFFile const *cdf, long const &var_num);
    [[nodiscard]] auto buffer_size() const noexcept -> std::size_t;

public:
    auto operator>>(WSLINK link) const -> CDFzVar const &;

private:
    auto operator>>(std::vector<unsigned char> &) const -> CDFzVar const &;
    auto operator>>(std::vector<long> &) const -> CDFzVar const &;
    auto operator>>(std::vector<double> &) const -> CDFzVar const &;
    auto operator>>(std::vector<epoch_t> &) const -> CDFzVar const &;

    template <class T>
    void load(std::vector<T> &) const;

public:
    [[nodiscard]] auto attrs() const -> std::vector<CDFvAttr>;
};

// CDFvAttr
//
class [[nodiscard]] CDFvAttr {
    friend CDFzVar;

    CDFFile const *m_cdf;
    long           m_attr_num;
    long           m_entry_num;
    std::string    m_name;
    long           m_type{};
    long           m_n_elems{};

    explicit CDFvAttr(CDFFile const *cdf, long const &attr_num, long const &entry_num);

public:
    auto operator>>(WSLINK link) const -> CDFvAttr const &;

private:
    auto operator>>(std::vector<unsigned char> &buffer) const -> CDFvAttr const &;
    auto operator>>(std::vector<long> &buffer) const -> CDFvAttr const &;
    auto operator>>(std::vector<double> &buffer) const -> CDFvAttr const &;
    auto operator>>(std::vector<epoch_t> &buffer) const -> CDFvAttr const &;

    template <class T>
    void load(std::vector<T> &buffer) const;
};

// print
//
template <class CharT, class Traits, class Arg>
inline void print(std::basic_ostream<CharT, Traits> &os, Arg &&arg)
{
    os << arg;
}
template <class CharT, class Traits, class First, class... Rest>
inline void print(std::basic_ostream<CharT, Traits> &os, First &&_1, Rest &&..._n)
{
    print(os << _1, std::forward<Rest>(_n)...);
}

template <class First, class... Rest>
inline std::string sprint(First &&_1, Rest &&..._n)
{
    std::stringstream os;
    print(os, std::forward<First>(_1), std::forward<Rest>(_n)...);
    return os.str();
}
