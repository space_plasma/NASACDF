/*
 * Copyright (c) 2016-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

extern void CDFListAttributes(const char *path);
extern void CDFImportAttributes(const char *path, const char *varname);
extern void CDFImportAttributes(const char *path);
