/*
 * Copyright (c) 2016-2024, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "CDFLink-Attributes.h"
#include "CDFHelper.h"

void CDFListAttributes(const char *path)
try {
    [cdf = CDFFile{ path }] {
        std::vector<std::string> const names = cdf.attr_names();
        {
            WSPutFunction(stdlink, "List", int(names.size()));
            for (auto const &name : names) {
                WSPutString(stdlink, name.c_str());
            }
        }
    }();
} catch (std::exception &e) {
    std::string msg = sprint("Message[CDFImport::err, \"", e.what(), "\"]");
    WSEvaluateString(stdlink, msg.data());
    WSPutFunction(stdlink, "Abort", 0);
}

void CDFImportAttributes(const char *path, const char *varname)
try {
    [cdf = CDFFile{ path }, varname] {
        std::vector<CDFvAttr> const attrs = cdf.zvar(varname).attrs();
        {
            WSPutFunction(stdlink, "List", int(attrs.size()));
            for (auto const &attr : attrs) {
                attr >> stdlink;
            }
        }
    }();
} catch (std::exception &e) {
    std::string msg = sprint("Message[CDFImport::err, \"", e.what(), "\"]");
    WSEvaluateString(stdlink, msg.data());
    WSPutFunction(stdlink, "Abort", 0);
}

void CDFImportAttributes(const char *path)
try {
    [cdf = CDFFile{ path }] {
        std::vector<CDFgAttr> const attrs = cdf.attrs();
        {
            WSPutFunction(stdlink, "List", int(attrs.size()));
            for (auto const &attr : attrs) {
                attr >> stdlink;
            }
        }
    }();
} catch (std::exception &e) {
    std::string msg = sprint("Message[CDFImport::err, \"", e.what(), "\"]");
    WSEvaluateString(stdlink, msg.data());
    WSPutFunction(stdlink, "Abort", 0);
}
