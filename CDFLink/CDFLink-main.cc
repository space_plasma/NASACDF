/*
 * Copyright (c) 2016-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <csignal>
#include <cstdlib>
#include <cstring>
#include <filesystem>
#include <iostream>
#include <string_view>
#include <wstp.h>

extern "C" WSEnvironment stdenv;

int main(int argc, char *argv[])
{
    // Set leap second path environment variable
    {
        using namespace std::string_view_literals;
        constexpr auto leap_seconds_env      = "CDF_LEAPSECONDSTABLE"sv;
        constexpr auto leap_seconds_filename = "CDFLeapSeconds.txt"sv;

        auto const leap_seconds_table_path = std::filesystem::canonical(std::filesystem::path{ *argv }.replace_filename(leap_seconds_filename));
        if (setenv(leap_seconds_env.data(), leap_seconds_table_path.c_str(), 1 /*overwrite*/))
            std::cerr << "\u001b[31m" << *argv << "\u001b[0m :: Setting " << leap_seconds_env << " environment variable failed: " << strerror(errno) << '\n';
        else
            std::cout << "\u001b[36m" << *argv << "\u001b[0m :: " << leap_seconds_env << " environment variable set: " << getenv(leap_seconds_env.data()) << std::endl;
    }

    // Reset unix signals; This should precede WSMain
    return [&] {
        if (!stdenv) {
            WSEnvironmentParameter const envP = WSNewParameters(WSREVISION, WSAPIREVISION);
            if (!envP) {
                std::cerr << "\u001b[31m" << *argv << "\u001b[0m :: Unable to create environment parameter object.\n";
                return 1;
            }

            // disable signal handling
            if (WSEOK != WSDoNotHandleSignalParameter(envP, SIGTERM))
                std::cerr << "\u001b[31m" << *argv << "\u001b[0m :: Failed to disable SIGTERM signal handling.\n";
            if (WSEOK != WSDoNotHandleSignalParameter(envP, SIGINT))
                std::cerr << "\u001b[31m" << *argv << "\u001b[0m :: Failed to disable SIGINT signal handling.\n";

            stdenv = WSInitialize(envP);
            WSReleaseParameters(envP);
            if (!stdenv) {
                std::cerr << "\u001b[31m" << *argv << "\u001b[0m :: Unable to create environment object.\n";
                return 1;
            }
        }
        return WSMain(argc, argv);
    }();
}
